# CanviaJavaChallenge

## Problema 1 Java loop

Objetivo
En este desafío, vamos a usar bucles para ayudarnos a hacer algunas matemáticas simples.

Tarea
Dado un número entero N, imprime sus primeros 10 múltiplos. Cada múltiplo N x i 
(donde 1 <= i <= 10) debe imprimirse en una nueva línea en la forma: N x i = resultado.

Entrada
 Un simple entero N.

Restricciones
 2 <= N <= 20

Formato de salida
Imprimir 10 lineas de salida

Entrada de ejemplo

2

Salida de ejemplo

 2 x 1 = 2<br/>
 2 x 2 = 4<br/>
 2 x 3 = 6<br/>
 2 x 4 = 8<br/>
 2 x 5 = 10<br/>
 2 x 6 = 12<br/>
 2 x 7 = 14<br/>
 2 x 8 = 16<br/>
 2 x 9 = 18<br/>
 2 x 10 = 20


## Problema 2 Java Reverse

Un palíndromo es una palabra, frase, número u otra secuencia de caracteres que se lee igual hacia atrás o hacia adelante (Wikipedia).

Dado un string A, imprima Si, si es un palindrome, imprima No si no lo es.

Ejemplo de entrada:

madam

Ejemplo de salida:

Si

## Problema 3 Java herencia

Usando la herencia, una clase puede adquirir las propiedades de otras. Considere la siguiente clase de animales:


```java
class Animal{
    void camina(){
        System.out.println("Estoy caminando");
    }
}
```

Esta clase tiene solo un método, caminar. A continuación, queremos crear una clase Pajaro que también tenga un método volar. Hacemos esto usando la palabra clave extend:

```java
class Pajaro extends Animal {
    void fly() {
        System.out.println("Estoy volando");
    }
}
```

Finalmente, podemos crear un objeto Pajaro que pueda volar y caminar.

```java
public class Solution{
   public static void main(String[] args){

      Bird bird = new Bird();
      bird.walk();
      bird.fly();
   }
}
```

Se imprimirá el código anterior:

Estoy caminando
Estoy volando

Esto significa que un objeto Pajaro tiene todas las propiedades que tiene un objeto Animal, así como algunas propiedades únicas adicionales.

El código anterior se proporciona en su editor. Debe agregar un método cantar a la clase Pajaro, luego modificar el método main en consecuencia para que el código imprima las siguientes líneas:

Estoy caminando
Estoy volando
Estoy cantando
