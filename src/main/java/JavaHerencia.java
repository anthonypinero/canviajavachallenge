
    class Animal{
        void caminar(){
            System.out.println("Estoy caminando");
        }
    }


    class Pajaro extends Animal {
        void volar(){
            System.out.println("Estoy volando");
        }
    }


    public class JavaHerencia {
        public static void main(String args[]){

            Pajaro bird = new Pajaro();
            bird.caminar();
            bird.volar();

        }
}


